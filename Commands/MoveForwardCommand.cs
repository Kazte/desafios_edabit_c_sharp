﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Desafios_Edabit.Commands
{
    public class MoveForwardCommand:Command
    {
        public MoveForwardCommand(string name) : base(name)
        {

        }

        public override void Do(Robot robot)
        {
            switch (robot.direction)
            {
                case Direction.North:
                    robot.MovePosition(0, 1);
                    break;
                case Direction.South:
                    robot.MovePosition(0, -1);
                    break;
                case Direction.West:
                    robot.MovePosition(-1, 0);
                    break;
                case Direction.East:
                    robot.MovePosition(1, 0);
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
    }
}
