﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Desafios_Edabit
{
    public class Command
    {
        public string commandName { get; private set; }

        public Command(string name)
        {
            commandName = name;
        }

        public virtual void Do(Robot robot)
        {

        }
    }
}
