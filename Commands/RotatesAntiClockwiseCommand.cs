﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Desafios_Edabit.Commands
{
    public class RotatesAntiClockwiseCommand : Command

    {
    public RotatesAntiClockwiseCommand(string name) : base(name)
    {
    }

    public override void Do(Robot robot)
    {
        switch (robot.direction)
        {
            case Direction.North:
                robot.SetDirection(Direction.West);
                break;
            case Direction.South:
                robot.SetDirection(Direction.East);
                break;
            case Direction.West:
                robot.SetDirection(Direction.South);
                break;
            case Direction.East:
                robot.SetDirection(Direction.North);
                break;
            default:
                throw new ArgumentOutOfRangeException();
        }
    }
    }
}
