﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Desafios_Edabit
{
    internal class DesafiosAvanzado
    {
        public string ReverseAndNot(int i)
        {
            /*
             * Write a function that takes an integer i and returns a
             * string with the integer backwards followed by the original integer.
             */

            string normalString = "";

            for (int j = i.ToString().Length - 1; j >= 0; j--)
            {
                normalString += i.ToString()[j];
            }

            normalString += i.ToString();

            return normalString;
        }


        public string UncensorString(string word, string vowels)
        {
            /*
             * Someone has attempted to censor my strings by replacing every vowel with a *, l*k* th*s.
             * Luckily, I've been able to find the vowels that were removed.
             * Given a censored string and a string of the censored vowels, return the original uncensored string.
             */

            Char censorCharacter = '*';

            string newString = "";

            int indexWord = 0;
            int indexVowels = 0;


            while (indexWord < word.Length)
            {
                Char currentChar = word[indexWord];

                if (currentChar == censorCharacter)
                {
                    newString += vowels[indexVowels];
                    indexVowels++;
                }
                else
                {
                    newString += currentChar;
                }

                indexWord++;
            }
            

            return newString;
        }

        public string Interview(int[] solveTimes, int totalTime)
        {
            /*
             * Create a function to check if a candidate is qualified in an imaginary coding interview of an imaginary tech startup.

                The criteria for a candidate to be qualified in the coding interview is:

                The candidate should have complete all the questions.
                The maximum time given to complete the interview is 120 minutes.
                The maximum time given for very easy questions is 5 minutes each.
                The maximum time given for easy questions is 10 minutes each.
                The maximum time given for medium questions is 15 minutes each.
                The maximum time given for hard questions is 20 minutes each.
                If all the above conditions are satisfied, return "qualified", else return "disqualified".

                You will be given an array of time taken by a candidate to solve a particular question and the total time taken by the candidate to complete the interview.

                Given an array, in a true condition will always be in the format 
                [very easy, very easy, easy, easy, medium, medium, hard, hard].

                The maximum time to complete the interview includes a buffer time of 20 minutes.
             */

            if (totalTime > 120)
                return "disqualified";

            if (solveTimes.Length != 8)
                return "disqualified";

            // Very Easy
            if (solveTimes[0] > 5 || solveTimes[1] > 5)
                return "disqualified";

            // Easy
            if (solveTimes[2] > 10 || solveTimes[3] > 10)
                return "disqualified";


            // Medium
            if (solveTimes[4] > 15 || solveTimes[5] > 15)
                return "disqualified";

            // Hard
            if (solveTimes[6] > 20 || solveTimes[7] > 20)
                return "disqualified";



            return "qualified";
        }

        public bool ConsecutiveNumbers(int[] numbers)
        {
            /*
             * Create a function that determines whether elements in an array can be re-arranged to
             * form a consecutive list of numbers where each number appears exactly once.
             */

            var sortNumbers = numbers.OrderBy(x => x).ToList();

            var result = true;

            var i = 0;

            while (i < sortNumbers.Count() && result)
            {
                if (i > 0 && sortNumbers[i] - sortNumbers[i - 1] != 1)
                {
                    result = false;
                }

                i++;
            }

            return result;
        }

        public bool IsValidHexCode(string hexCode)
        {
            /*
             * Create a function that determines whether a string is a valid hex code.

                A hex code must begin with a pound key # and is exactly 6 characters in length. 
                Each character must be a digit from 0-9 or an alphabetic character from A-F. 
                All alphabetic characters may be uppercase or lowercase.
             */

            var result = true;

            Char poundKey = '#';

            string numbers = "0123456789";
            string letters = "abcdef";

            if (hexCode[0] != poundKey || hexCode.Count() > 7)
                result = false;

            hexCode = hexCode.ToLower();

            var i = 1;

            while (i < hexCode.Count() && result)
            {
                if (!numbers.Contains(hexCode[i]) && !letters.Contains(hexCode[i])) {
                    result = false;
                }

                i++;
            }

            return result;
        }

        public bool IsSmooth(string word)
        {
            /*
             * Carlos is a huge fan of something he calls smooth sentences.

                A smooth sentence is one where the last letter of each word is identical to the first letter 
                the following word (and not case sensitive, so "A" would be the same as "a").

                The following would be a smooth sentence "Carlos swam masterfully" because "Carlos" 
                ends with an "s" and swam begins with an "s" and swam ends with an "m" and masterfully begins with an "m".
    
                Create a function that determines whether the input sentence is a smooth sentence, 
                returning a boolean value true if it is, false if it is not.
             */

            var wordList = word.ToLower().Split(' ');

            var result = true;

            var i = 0;

            while (i < wordList.Count() && result)
            {
                if (i < wordList.Length - 1 && wordList[i].Last() != wordList[i + 1][0])
                {
                    result = false;
                }

                i++;
            }

            return result;
        }
    }
}
