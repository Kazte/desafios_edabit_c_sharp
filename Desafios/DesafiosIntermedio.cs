﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Desafios_Edabit.Intermedio
{
    internal class DesafiosIntermedio
    {
        public int[] ArrayOfMultiples(int num, int len)
        {
            /*
             * Create a function that takes two numbers as arguments (num, length)
             * and returns an array of multiples of num until the array length reaches length.
             */

            int[] array = new int[len];

            for (int i = 1; i <= len; i++)
            {
                array[i - 1] = num * i;
            }


            return array;
        }

        public string ReverseCase(string s)
        {
            /*
             * Given a string, create a function to reverse the case.
             * All lower-cased letters should be upper-cased, and vice versa.
             */

            return s.Aggregate("", (current, c) => current + (char.IsLower(c) ? char.ToUpper(c) : char.ToLower(c)));
        }

        public bool CheckEquality(object a, object b)
        {
            /*
             * In this challenge, you must verify the equality of two different values given the parameters a and b.
               
               Both the value and type of the parameters need to be equal. The possible types of the given parameters are:
               
               Numbers
               Strings
               Booleans (false or true)
               What have you learned so far that will permit you to do two different checks (value and type) with a single statement?
               
               Implement a function that returns true if the parameters are equal, and false if they are not.
             */


            return a.Equals(b);
        }

        public List<int> IndexOfCapitals(string s)
        {
            List<int> indexes = new List<int>();

            var currentIndex = 0;
            while (currentIndex < s.Length)
            {
                if (char.IsUpper(s[currentIndex]))
                {
                    indexes.Add(currentIndex);
                }

                currentIndex++;
            }

            return indexes;
        }

        public string Bomb(string s)
        {
            /*
             * Create a function that finds the word "bomb" in the given string (not case sensitive).
             * If found, return "Duck!!!", otherwise, return "There is no bomb, relax.".
             */

            

            return s.Contains("bomb") ? "Duck!!!" : "There is no bomb, relax.";
        }

        public string[] ParseArray(object[] array)
        {
            /*
             * Create a function that takes an array of integers and strings, converts integers to strings,
             * and returns the array as a string array.
             */


            return array.Select(x => x.ToString()).ToArray();
        }
    }
}
