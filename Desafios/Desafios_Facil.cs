﻿using System;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;

namespace Desafios_Edabit.Facil
{
    public class Desafios_Facil
    {
        public string MonthName(int month)
        {
            /*
             * Create a function that takes a number (from 1 to 12) and returns its corresponding month name as a string.
             * For example, if you're given 3 as input, your function should return "March", because March is the 3rd month.
             */

            var months = new string[] { 
                "January",
                "February",
                "March",
                "April",
                "May",
                "June",
                "July",
                "August",
                "September",
                "October",
                "November",
                "December" 
            };

            if (month <= 0 || month > months.Length)
                return null;


            return months[month - 1];
        }

        public int[] MinMax(int[] numbers)
        {
            /*
             * Create a function that takes an array of numbers and return both the minimum and maximum numbers, in that order.
             */

            var min = numbers.Min();
            var max = numbers.Max();

            return new []{min, max};
        }

        public int AbsoluteSum(int[] numbers)
        {
            /*
             * Take an array of integers (positive or negative or both)
             * and return the sum of the absolute value of each element.
             */
            
            return numbers.Select(e => e >= 0 ? e : -e).Sum();
        }

        public int Power(int b, int exp)
        {
            var total = 1;

            for (var i = 0; i < exp; i++)
            {
                total *= b;
            }

            return total;
        }


        public int[] MultiplyByLength(int[] numbers)
        {
            /*
             * Create a function to multiply all of the values in an array by the amount of values in the given array.
             */

            return numbers.Select(e => e * numbers.Length).ToArray();
        }

        public int HammingDistance(string stringA, string stringB)
        {
            /*
             * Hamming distance is the number of characters that differ between two strings.
             */

            return stringA.Zip(stringB, (a, b) => (a == b ? 0 : 1)).Sum();
        }
    }
}