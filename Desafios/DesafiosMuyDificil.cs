﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Desafios_Edabit.Desafios
{
    public class DesafiosMuyDificil
    {
        public string FiboWord(int n)
        {
            if (n < 2)
                return "invalid";

            var finalString = "";

            var a = "b";
            var b = "a";

            finalString = "b, a";

            for (var i = 0; i < n - 2; i++)
            {
                var aux = a;
                a = b;
                b = b + aux;

                finalString += ", " + b;
            }

            return finalString;
        }

        public string OverTime(decimal[] array)
        {
            var totalHours = Math.Abs(array[1]-array[0]);

            var extraHours = array[1] - 17;

            var total = 0m;

            if (extraHours > 0)
            {
                totalHours = totalHours - extraHours;
                total = extraHours * array[2] * array[3];
            }

            total += totalHours * array[2];
            
            var totalString = total.ToString("00.00").Replace(',','.');

            return $"${totalString}";
        }

        public int License(string me, int agents, string others)
        {
            var allNames = others.Split(' ').ToList();
            allNames.Add(me);

            var flag = false;
            var totalTime = 0;

            allNames.Sort();

            foreach (var name in allNames)
            {
                Console.WriteLine(name);
            }

            var myPos = allNames.IndexOf(me) + 1;

            var customers = 0;

            while (customers < myPos)
            {
                totalTime += 20;
                customers += agents;
            }


            return totalTime;
        }

        public int[] TrackRobot(string commands)
        {
            var robot = new Robot();

            foreach (var command in commands)
            {
                robot.ExecuteCommand(command.ToString());
            }


            return robot.position;
        }
    }

    public struct Rational
    {
        /*
         * C# has several numeric types, including natural, real, and irrational numbers.
         * One numeric type that's missing is a Rational number.
         * A rational number, as the name suggests is a ratio between 2 natural numbers and is
         * usually represented as a fraction in the form 1/2, 5/4, -79/13, etc.
         * Create a C# struct with a constructor that takes two integer parameters,
         * either or both of which may be positive or negative. Include two read-only properties,
         * Numerator and Denominator, that return the numerator and denominator of the fraction
         * respectively of type int.
         * Also, override the ToString() method to give a string representation of the rational
         * number as described in the preceding paragraph.
         */
        public int numerator { get; private set; }

        public int denominator { get; private set; }

        public Rational(int numerator, int denominator)
        {
            if (denominator < 0)
            {
                numerator *= -1;
                denominator *= -1;
            }

            var minValue = Math.Min(Math.Abs(numerator), denominator);
            var mcd = 0;

            for (var i = minValue; i > 0; i--)
            {
                if (numerator % i == 0 && denominator % i == 0)
                {
                    mcd = i;
                    break;
                }
            }

            if (mcd > 0)
            {
                numerator /= mcd;
                denominator /= mcd;
            }

            this.numerator = numerator;
            this.denominator = denominator;
        }

        public override string ToString()
        {
            if (denominator == 1)
                return $"{numerator}";

            return $"{numerator}/{denominator}";
        }
    }
}
