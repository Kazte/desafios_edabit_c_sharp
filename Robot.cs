﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Desafios_Edabit.Commands;

namespace Desafios_Edabit
{
    public class Robot
    {
        public int[] position { get; private set; }

        public Direction direction { get; private set; }

        private List<Command> commands;

        public Robot()
        {
            position = new[] { 0, 0 };
            direction = Direction.East;

            commands = new List<Command>();

            commands.Add(new MoveForwardCommand("."));
            commands.Add(new RotatesClockwiseCommand(">"));
            commands.Add(new RotatesAntiClockwiseCommand("<"));
        }

        public void ExecuteCommand(string commandName)
        {
            var commandToExecute = commands.Find(c => c.commandName == commandName);
            commandToExecute.Do(this);
        }

        public void MovePosition(int px, int py)
        {
            position[0] += px;
            position[1] += py;
        }

        public void SetDirection(Direction newDirection)
        {
            direction = newDirection;
        }
    }

    //public interface IDirection
    //{
    //    int[] direction { get; set; }
    //}

    public enum Direction
    {
        North,
        South,
        West,
        East
    }
}
