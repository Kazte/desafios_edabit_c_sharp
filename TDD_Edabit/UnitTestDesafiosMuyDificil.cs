﻿using System;
using Desafios_Edabit.Desafios;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace TDD
{
    [TestClass]
    public class UnitTestDesafiosMuyDificil
    {
        [TestMethod]
        public void Test_Fibonacci_Words_Invalid()
        {
            var desafiosMuyDificil = new DesafiosMuyDificil();

            Assert.AreEqual("invalid", desafiosMuyDificil.FiboWord(1));
        }

        [TestMethod]
        public void Test_Fibonacci_Words_Valid_3()
        {
            var desafiosMuyDificil = new DesafiosMuyDificil();

            Assert.AreEqual("b, a, ab", desafiosMuyDificil.FiboWord(3));
        }

        [TestMethod]
        public void Test_Fibonacci_Words_Valid_7()
        {
            var desafiosMuyDificil = new DesafiosMuyDificil();

            Assert.AreEqual("b, a, ab, aba, abaab, abaababa, abaababaabaab", desafiosMuyDificil.FiboWord(7));
        }


        [TestMethod]
        public void Test_Rational_Struct_Positives()
        {
            var desafiosMuyDificil = new DesafiosMuyDificil();

            var rational = new Rational(1, 2);
            
            Assert.AreEqual(1, rational.numerator);
            Assert.AreEqual(2, rational.denominator);
            Assert.AreEqual("1/2", rational.ToString());
        }

        [TestMethod]
        public void Test_Rational_Struct_Negatives()
        {
            var desafiosMuyDificil = new DesafiosMuyDificil();

            var rational = new Rational(2, -1);

            Assert.AreEqual(-2, rational.numerator);
            Assert.AreEqual(1, rational.denominator);
            Assert.AreEqual("-2", rational.ToString());
        }

        [TestMethod]
        public void Test_Rational_Struct_Reduced()
        {
            var desafiosMuyDificil = new DesafiosMuyDificil();

            var rational = new Rational(10, 8);

            Assert.AreEqual(5, rational.numerator);
            Assert.AreEqual(4, rational.denominator);
            Assert.AreEqual("5/4", rational.ToString());
        }

        [TestMethod]
        public void Test_Rational_Struct_Reduced_Negative()
        {
            var desafiosMuyDificil = new DesafiosMuyDificil();

            var rational = new Rational(-16, -64);

            Assert.AreEqual(1, rational.numerator);
            Assert.AreEqual(4, rational.denominator);
            Assert.AreEqual("1/4", rational.ToString());
        }

        [TestMethod]
        public void Test_Overtime_Regular_Hours()
        {
            var desafiosMuyDificil = new DesafiosMuyDificil();

            Assert.AreEqual("$240.00", desafiosMuyDificil.OverTime(new decimal[] { 9, 17, 30, 1.5m }));
        }

        [TestMethod]
        public void Test_Overtime_Regular_Hours_2()
        {
            var desafiosMuyDificil = new DesafiosMuyDificil();

            Assert.AreEqual("$84.00", desafiosMuyDificil.OverTime(new decimal[] { 16, 18, 30, 1.8m }));
        }


        [TestMethod]
        public void Test_Overtime_Regular_Hours_3()
        {
            var desafiosMuyDificil = new DesafiosMuyDificil();

            Assert.AreEqual("$52.50", desafiosMuyDificil.OverTime(new decimal[] { 13.25m, 15, 30, 1.5m }));
        }

        [TestMethod]
        public void Test_Driving_License_By_2_Agents()
        {
            var desafiosMuyDificil = new DesafiosMuyDificil();

            Assert.AreEqual(40, desafiosMuyDificil.License("Eric", 2, "Adam Caroline Rebecca Frank"));
        }

        [TestMethod]
        public void Test_Driving_License_By_1_Agents()
        {
            var desafiosMuyDificil = new DesafiosMuyDificil();

            Assert.AreEqual(100, desafiosMuyDificil.License("Zebediah", 1, "Bob Jim Becky Pat"));
        }

        [TestMethod]
        public void Test_Driving_License_By_3_Agents()
        {
            var desafiosMuyDificil = new DesafiosMuyDificil();

            Assert.AreEqual(20, desafiosMuyDificil.License("Aaron", 3, "Jane Max Olivia Sam"));
        }

        [TestMethod]
        public void Test_TrackRobot()
        {
            var desafiosMuyDificil = new DesafiosMuyDificil();

            var robotPosition = desafiosMuyDificil.TrackRobot("..<.<.");

            Assert.AreEqual(1, robotPosition[0]);
            Assert.AreEqual(1, robotPosition[1]);
        }
    }
}

