﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Desafios_Edabit.Desafios;
using Desafios_Edabit.Facil;
using Desafios_Edabit.Intermedio;

namespace Desafios_Edabit
{
    internal class Program
    {
        static void Main(string[] args)
        {
            
            // PruebasFacil();

            // PruebasIntermedio();

            PruebasAvanzado();

            var desafiosMuyDificil = new DesafiosMuyDificil();

            desafiosMuyDificil.License("Eric", 2, "Adam Caroline Rebecca Frank");

            Console.ReadLine();
        }

        private static void PruebasFacil()
        {
            var desafiosFacil = new Desafios_Facil();

            Console.WriteLine(desafiosFacil.MonthName(0));
            Console.WriteLine(desafiosFacil.MonthName(5));
            Console.WriteLine(desafiosFacil.MonthName(12));
            Console.WriteLine(desafiosFacil.MonthName(13));

            //

            var numbersMinMax = desafiosFacil.MinMax(new[] { 5, 11, 9, 3, 30, 55, 100 });
            Console.WriteLine(numbersMinMax[0]);
            Console.WriteLine(numbersMinMax[1]);

            //

            var numbersAbsSum = new int[] { -1, 2, -3 };

            Console.WriteLine(desafiosFacil.AbsoluteSum(numbersAbsSum));

            //

            Console.WriteLine(desafiosFacil.Power(5, 3));

            //

            var numbersMBL = desafiosFacil.MultiplyByLength(new[] { 2, 3, 1, 0 });

            foreach (var n in numbersMBL)
            {
                Console.WriteLine(n.ToString());
            }

            //
            Console.WriteLine(desafiosFacil.HammingDistance("abcde", "bcdef"));
            Console.WriteLine(desafiosFacil.HammingDistance("abcde", "abcde"));
        }

        private static void PruebasIntermedio()
        {

            var desafiosIntermedio = new DesafiosIntermedio();

            var numbersMult = desafiosIntermedio.ArrayOfMultiples(17, 6);
            foreach (var n in numbersMult)
            {
                Console.WriteLine(n);
            }

            //

            Console.WriteLine(desafiosIntermedio.ReverseCase("sPoNtAnEoUs"));

            //

            Console.WriteLine(desafiosIntermedio.CheckEquality(1, true));
            Console.WriteLine(desafiosIntermedio.CheckEquality(0, "0"));
            Console.WriteLine(desafiosIntermedio.CheckEquality(1,1));

            //

            var indexCap = desafiosIntermedio.IndexOfCapitals("eDaBiT");

            foreach (var i in indexCap)
            {
                Console.WriteLine(i);
            }

            //

            Console.WriteLine(desafiosIntermedio.Bomb("There is a bomb."));

            var array = desafiosIntermedio.ParseArray(new object[] { 1, true, 2, "a", "b" });

            foreach (string a in array)
            {
                Console.WriteLine(a);
            }
        }

        private static void PruebasAvanzado()
        {
            var desafiosAvanzado = new DesafiosAvanzado();


            Console.WriteLine(desafiosAvanzado.ReverseAndNot(123));

            Console.WriteLine(desafiosAvanzado.ReverseAndNot(152));

            Console.WriteLine(desafiosAvanzado.ReverseAndNot(123456789));

            //

            Console.WriteLine(desafiosAvanzado.UncensorString("Wh*r* d*d my v*w*ls g*?", "eeioeo"));
            Console.WriteLine(desafiosAvanzado.UncensorString("abcd", ""));
            Console.WriteLine(desafiosAvanzado.UncensorString("*PP*RC*S*", "UEAE"));

            //

            Console.WriteLine(desafiosAvanzado.Interview(new int[] { 5, 5, 10, 10, 15, 15, 20, 20 }, 120));
                
            Console.WriteLine(desafiosAvanzado.Interview(new int[] { 2, 3, 8, 6, 5, 12, 10, 18 }, 64));

            Console.WriteLine(desafiosAvanzado.Interview(new int[] { 5, 5, 10, 10, 25, 15, 20, 20 }, 120));

            Console.WriteLine(desafiosAvanzado.Interview(new int[] { 5, 5, 10, 10, 15, 15, 20 }, 120));

            Console.WriteLine(desafiosAvanzado.Interview(new int[] { 5, 5, 10, 10, 15, 15, 20, 20 }, 130));

            //


            Console.WriteLine(desafiosAvanzado.ConsecutiveNumbers(new int[]{ 5, 1, 4, 3, 2 }));

            Console.WriteLine(desafiosAvanzado.ConsecutiveNumbers(new int[]{5, 1, 4, 3, 2, 8}));

            Console.WriteLine(desafiosAvanzado.ConsecutiveNumbers(new int[]{5, 6, 7, 8, 9, 9}));
            
            //


            Console.WriteLine(desafiosAvanzado.IsValidHexCode("#CD5C5C"));

            Console.WriteLine(desafiosAvanzado.IsValidHexCode("#EAECEE"));

            Console.WriteLine(desafiosAvanzado.IsValidHexCode("#eaecee"));

            Console.WriteLine(desafiosAvanzado.IsValidHexCode("#CD5C58C"));

            Console.WriteLine(desafiosAvanzado.IsValidHexCode("#CD5C5Z"));

            Console.WriteLine(desafiosAvanzado.IsValidHexCode("#CD5C&C"));

            Console.WriteLine(desafiosAvanzado.IsValidHexCode("CD5C5C"));

            //

            Console.WriteLine(desafiosAvanzado.IsSmooth("Marta appreciated deep perpendicular right trapezoids"));

            Console.WriteLine(desafiosAvanzado.IsSmooth("Someone is outside the doorway") );

            Console.WriteLine(desafiosAvanzado.IsSmooth("She eats super righteously"));

        }
    }
}

